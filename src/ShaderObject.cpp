#include "ShaderObject.hpp"

#include <iostream>

namespace amn
{
ShaderObject::ShaderObject(Type shaderType, const std::basic_string<GLchar>& shaderSource)
{
	std::cout << "Compiling shader of type: " << static_cast<GLenum>(shaderType) << "\n";

	self = glCreateShader(static_cast<GLenum>(shaderType));
	const GLchar* source = shaderSource.data();
	glShaderSource(self, 1, &source, nullptr);
	glCompileShader(self);

	GLint isCompiled = 0;
	glGetShaderiv(self, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint logLength = 0;
		glGetShaderiv(self, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength < 0)
		{
			logLength = 0;
		}

		std::basic_string<GLchar> log{};
		log.resize(static_cast<size_t>(logLength));

		glGetShaderInfoLog(self, logLength, nullptr, log.data());

		glDeleteShader(self);

		throw std::runtime_error(
			"Failed to compile shader, of type: " + std::to_string(static_cast<GLenum>(shaderType))
			+ ", due to: " + log);
	}

	std::cout << "Successfully compiled shader.\n";

}

ShaderObject::~ShaderObject()
{
	glDeleteShader(self);
}
} /* amn */ 
