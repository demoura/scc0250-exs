#include "VertexArray.hpp"

namespace amn
{

VertexArray::AttributeSpec::AttributeSpec(Floating type, GLint count) :
	mode(Mode::Float), type(static_cast<GLenum>(type)), numComponents(count), normalized(GL_FALSE)
{
}

VertexArray::AttributeSpec::AttributeSpec(IntoFloat type, GLint count, bool normalized) :
	mode(Mode::Float), type(static_cast<GLenum>(type)), numComponents(count),
	normalized(normalized ? GL_TRUE : GL_FALSE)
{
}

VertexArray::AttributeSpec::AttributeSpec(Integer type, GLint count) :
	mode(Mode::Float), type(static_cast<GLenum>(type)), numComponents(count), normalized(GL_FALSE)
{
}

[[nodiscard]] auto VertexArray::AttributeSpec::Size() const -> std::size_t
{
	switch (type)
	{
	case GL_FLOAT: return sizeof(GLfloat) * static_cast<GLuint>(numComponents);
	case GL_HALF_FLOAT: return sizeof(GLhalf) * static_cast<GLuint>(numComponents);
	case GL_INT:
	case GL_INT_2_10_10_10_REV: return sizeof(GLint) * static_cast<GLuint>(numComponents);
	case GL_UNSIGNED_INT:
	case GL_UNSIGNED_INT_2_10_10_10_REV:
	case GL_UNSIGNED_INT_10F_11F_11F_REV:
		return sizeof(GLuint) * static_cast<GLuint>(numComponents);
	case GL_SHORT: return sizeof(GLshort) * static_cast<GLuint>(numComponents);
	case GL_UNSIGNED_SHORT: return sizeof(GLushort) * static_cast<GLuint>(numComponents);
	case GL_BYTE: return sizeof(GLbyte) * static_cast<GLuint>(numComponents);
	case GL_UNSIGNED_BYTE: return sizeof(GLubyte) * static_cast<GLuint>(numComponents);
	case GL_DOUBLE: return sizeof(GLdouble) * static_cast<GLuint>(numComponents);
	case GL_FIXED: return sizeof(GLfixed) * static_cast<GLuint>(numComponents);
	default: return 0; // unreachable
	};
}

VertexArray::VertexArray() { glGenVertexArrays(1, &vao); }

VertexArray::~VertexArray() { glDeleteVertexArrays(1, &vao); }

// NOLINTNEXTLINE(readability-make-member-function-const)
auto VertexArray::Bind() -> void { glBindVertexArray(vao); }

auto VertexArray::DisableAttributeArray(GLuint index) -> void
{
	Bind();
	glDisableVertexAttribArray(index);
};

auto VertexArray::EnableAttributeArray(GLuint index) -> void
{
	Bind();
	glEnableVertexAttribArray(index);
};

} // namespace amn
