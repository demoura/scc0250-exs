#include "Vector3.hpp"

#include <cmath>

#include "Vector2.hpp"
#include "Vector4.hpp"

namespace amn
{

Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z) {}
Vector3::Vector3(const Vector2& vec) : x(vec.x), y(vec.y) {}
Vector3::Vector3(const Vector4& vec) : x(vec.x), y(vec.y), z(vec.z) {}
Vector3::Vector3(const Vector2& vec, float z) : x(vec.x), y(vec.y), z(z) {}

auto Vector3::operator+=(Vector3 a) -> Vector3&
{
    x += a.x;
    y += a.y;
	z += a.z;
    return *this;
}

auto Vector3::operator-=(Vector3 a) -> Vector3&
{
    x -= a.x;
    y -= a.y;
	z -= a.z;
    return *this;
}

auto Vector3::operator+=(float a) -> Vector3&
{
    x += a;
    y += a;
    z += a;
    return *this;
}

auto Vector3::operator-=(float a) -> Vector3&
{
    x -= a;
    y -= a;
    z -= a;
    return *this;
}

auto Vector3::operator*=(float a) -> Vector3&
{
    x *= a;
    y *= a;
	z *= a;
    return *this;
}

auto Vector3::operator/=(float a) -> Vector3&
{
    x /= a;
    y /= a;
	z /= a;
    return *this;
}

auto Vector3::operator-() const -> Vector3
{
    return {-x, -y, -z};
}

auto Vector3::ClampMagnitude(float max) -> Vector3
{
    if (SqrMagnitude() > max * max)
    {
        return Normalized() * max;
    }
    
    return *this;
}

auto Vector3::SqrMagnitude() const -> float
{
    return x * x + y * y + z * z; 
}

auto Vector3::Dot(Vector3 a) const -> float
{
    return x * a.x + y * a.y + z * a.z;
}

auto Vector3::Cross(Vector3 a) const -> Vector3
{
    return { y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x }; 
}

auto Vector3::Magnitude() const -> float
{
    return std::sqrt(x * x + y * y + z * z); 
}

auto Vector3::Normalize() -> Vector3&
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        *this /= mag;        
    }

    return *this;
}

auto Vector3::Normalized() const -> Vector3
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        return *this / mag;        
    }

    return {};
}

auto operator+(Vector3 a, Vector3 b) -> Vector3
{
    return {a.x + b.x, a.y + b.y, a.z + b.z};
}

auto operator-(Vector3 a, Vector3 b) -> Vector3
{
    return {a.x - b.x, a.y - b.y, a.z - b.z};
}

auto operator*(Vector3 a, float b) -> Vector3
{
    return {a.x * b, a.y * b, a.z * b};
}

auto operator*(float a, Vector3 b) -> Vector3
{
    return b * a;
}

auto operator/(Vector3 a, float b) -> Vector3
{
    return {a.x / b, a.y / b, a.z / b};
}

auto operator==(Vector3 a, Vector3 b) -> bool
{
    return a.x == b.x && a.y == b.y && a.z == b.z;
}

auto operator!=(Vector3 a, Vector3 b) -> bool
{
    return a.x != b.x || a.y != b.y || a.z != b.z;
}

}
