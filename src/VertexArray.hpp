#ifndef AMN_VERTEX_ARRAY_HPP
#define AMN_VERTEX_ARRAY_HPP

#include <GL/glew.h>
#include <vector>

#include "VertexBuffer.hpp"

namespace amn
{
	
class VertexArray
{
public:
	enum class Floating : GLenum
	{
		HalfFloat = GL_HALF_FLOAT,
		Float = GL_FLOAT,
		Double = GL_DOUBLE,
		Fixed = GL_FIXED,
	};

	enum class IntoFloat : GLenum
	{
		Byte = GL_BYTE,
		UByte = GL_UNSIGNED_BYTE,
		Short = GL_SHORT,
		UShort = GL_UNSIGNED_SHORT,
		Int = GL_INT,
		UInt = GL_UNSIGNED_INT,
		Int2_10_10_10_Rev = GL_INT_2_10_10_10_REV,
		UInt2_10_10_10_Rev = GL_UNSIGNED_INT_2_10_10_10_REV,
		UInt_10f_11f_1ff_Rev = GL_UNSIGNED_INT_10F_11F_11F_REV,
	};

	enum class Integer : GLenum
	{
		Byte = GL_BYTE,
		UByte = GL_UNSIGNED_BYTE,
		Short = GL_SHORT,
		UShort = GL_UNSIGNED_SHORT,
		Int = GL_INT,
		UInt = GL_UNSIGNED_INT,
	};

	struct AttributeSpec
	{
		friend class VertexArray;

		enum class Mode
		{
			Float,
			Integer,
		};

		AttributeSpec(Floating type, GLint count);

		AttributeSpec(IntoFloat type, GLint count, bool normalized);

		AttributeSpec(Integer type, GLint count);

		[[nodiscard]] auto Size() const -> std::size_t;
	private:
		Mode mode;
		GLenum type;
		GLint numComponents;
		GLboolean normalized;
	};

public:
	VertexArray();

	VertexArray(VertexArray&& other) = default;

	auto operator=(VertexArray&& other) -> VertexArray& = default;

	~VertexArray();
public:
	auto Bind() -> void;

	auto DisableAttributeArray(GLuint index) -> void;

	auto EnableAttributeArray(GLuint index) -> void;

	template<typename T>
	auto SetAttributePointers(VertexBuffer<T>& vbo, std::size_t baseOffset) -> void;

private:
	GLuint vao;
};

template<typename T>
auto VertexArray::SetAttributePointers(VertexBuffer<T>& vbo, std::size_t baseOffset) -> void
{
	static_assert(std::is_same_v<typename decltype(T::Specs)::value_type, VertexArray::AttributeSpec>,
			"Type T must have a static container of VertexArray::AttributeSpec: \n"
			"\tExample: static const std::vector<VertexArray::AttributeSpec> Specs;");
	Bind();

	vbo.Bind();

	GLuint location = 0;
	GLuint buffer_offset = 0;
	for (const AttributeSpec& spec : T::Specs)
	{
		if (spec.mode == AttributeSpec::Mode::Float)
		{
			glVertexAttribPointer(
				location,
				spec.numComponents,
				spec.type,
				spec.normalized,
				sizeof(T),
				reinterpret_cast<const void*>(baseOffset + buffer_offset));
		}
		else if (spec.mode == AttributeSpec::Mode::Integer)
		{
			glVertexAttribIPointer(
				location,
				spec.numComponents,
				spec.type,
				sizeof(T),
				reinterpret_cast<const void*>(baseOffset + buffer_offset));
		}
		/* location += static_cast<GLuint>(spec.numComponents); */
		location += 1;
		buffer_offset += static_cast<GLuint>(spec.Size());
	}
}

} /* amn */ 

#endif /* end of include guard: AMN_VERTEX_ARRAY_HPP */
