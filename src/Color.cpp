#include "Color.hpp"

namespace amn
{

auto Color::RedClamped() const -> GLclampf
{
	return static_cast<GLclampf>(static_cast<float>(rgbaBytes[3]) / 255.0f);
}

auto Color::GreenClamped() const -> GLclampf
{
	return static_cast<GLclampf>(static_cast<float>(rgbaBytes[2]) / 255.0f);
}

auto Color::BlueClamped() const -> GLclampf
{
	return static_cast<GLclampf>(static_cast<float>(rgbaBytes[1]) / 255.0f);
}

auto Color::AlphaClamped() const -> GLclampf
{
	return static_cast<GLclampf>(static_cast<float>(rgbaBytes[0]) / 255.0f);
}

}
