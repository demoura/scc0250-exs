#ifndef AMN_UTIL_HPP
#define AMN_UTIL_HPP

#include <array>
#include <vector>
#include <string>

#include <GL/glew.h>

#include "Vector2.hpp"

namespace amn
{
namespace mathf
{
constexpr float Pi = 3.14159265358979323846f;
}

/// Loads a shader source from a file.
auto LoadShaderSource(const char* filename) -> std::basic_string<GLchar>;

/// Creates and compiles a shader.
auto BuildShader(GLenum shaderType, std::basic_string<GLchar> shaderSource) -> GLuint;

/// Creates and links a program.
auto BuildProgram(const std::vector<GLuint>& shaders) -> GLuint;

/// Loads data into an GL_ARRAY_BUFFER
template<typename Container>
auto LoadBuffer(const Container& vertices, GLuint buffer = 0) -> GLuint
{
	if (buffer == 0)
	{
		glGenBuffers(1, &buffer);
	}
	glBindBuffer(GL_ARRAY_BUFFER, buffer);

	glBufferData(
		GL_ARRAY_BUFFER,
		static_cast<GLsizei>(sizeof(typename Container::value_type) * vertices.size()),
		vertices.data(),
		GL_DYNAMIC_DRAW);

	return buffer;
}

auto GenerateCircle(Vector2 origin, float radius) -> std::array<Vector2, 32>;

} // namespace amn

#endif //AMN_UTIL_HPP
