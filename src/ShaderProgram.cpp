#include "ShaderProgram.hpp"

namespace amn
{
ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(self);
}

// NOLINTNEXTLINE(readability-make-member-function-const)
auto ShaderProgram::Bind() -> void
{
	glUseProgram(self);
}

auto ShaderProgram::SetUniform(GLint location, float value) -> void
{
	Bind();
	glUniform1f(location, value);
}

auto ShaderProgram::SetUniform(GLint location, GLint value) -> void
{
	Bind();
	glUniform1i(location, value);
}

auto ShaderProgram::SetUniform(GLint location, GLuint value) -> void
{
	Bind();
	glUniform1ui(location, value);
}

auto ShaderProgram::SetUniform(GLint location, Vector2 value) -> void
{
	Bind();
	glUniform2f(location, value.x, value.y);
}

auto ShaderProgram::SetUniform(GLint location, Vector3 value) -> void
{
	Bind();
	glUniform3f(location, value.x, value.y, value.z);
}

auto ShaderProgram::SetUniform(GLint location, Color value) -> void
{
	Bind();
	glUniform4f(location, value.RedClamped(), value.GreenClamped(), value.GreenClamped(), value.AlphaClamped());
}

auto ShaderProgram::SetUniform(GLint location, Matrix4x4 value, bool transpose) -> void
{
	Bind();
	glUniformMatrix4fv(location, 1, transpose ? GL_TRUE : GL_FALSE, reinterpret_cast<GLfloat*>(&value)); 
}

} /* amn */ 
