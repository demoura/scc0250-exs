#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstring>
#include <fstream>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <memory>
#include <ratio>
#include <stdexcept>
#include <string>
#include <system_error>

#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_surface.h>

#include "Color.hpp"
#include "Matrix4x4.hpp"
#include "ScopeGuard.hpp"
#include "ShaderObject.hpp"
#include "ShaderProgram.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"
#include "VertexArray.hpp"
#include "VertexBuffer.hpp"
#include "WavefrontParser.hpp"
#include "util.hpp"

namespace amn
{

// Declare constant values.
constexpr Color AsteroidColor{ 0xB3'B3'B3'FF };
constexpr Color BoltColor{ 0x00'D0'00'FF };
constexpr Color PlanetColor{ 0xED'C3'9A'FF };
constexpr Color ShipColor{ 0x1C'58'C7'FF };
constexpr Color SkyColor{ 0x1B'18'30'FF };
constexpr Color StarColor{ 0xFF'FF'FF'FF };

constexpr int WinWidth = 1280;
constexpr int WinHeight = 720;
constexpr const char* WinTitle = "cg03";

struct VertexInput
{
	Vector3 position;
	Vector2 texturePosition;
	static const std::vector<VertexArray::AttributeSpec> Specs;
};

const std::vector<VertexArray::AttributeSpec> VertexInput::Specs{
	{ VertexArray::Floating::Float, 3 },
	{ VertexArray::Floating::Float, 2 },
};

struct MeshRef
{
	GLenum mode;
	GLint first;
	GLsizei count;
};

auto LoadImage(const char* filename) -> std::unique_ptr<SDL_Surface, void (*)(SDL_Surface*)> 
{
	// Load texture
	std::unique_ptr<SDL_Surface, void (*)(SDL_Surface*)> image{
		IMG_Load(filename),
		// Custom deleter, so we don't leak memory, even if an exception is thrown.
		SDL_FreeSurface	
	};

	if (image == nullptr)
	{
		std::string msg{ "Failed to load image from file: " };
		msg.append(filename);
		msg.append("\n\tDue to: ");
		msg.append(IMG_GetError());

		throw std::runtime_error(msg);
	}

	/* std::unique_ptr<SDL_PixelFormat, void (*)(SDL_PixelFormat*)> format{ */
	/* 	SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), */
	/* 	SDL_FreeFormat */
	/* }; */

	/* if (format == nullptr) */
	/* { */
	/* 	throw std::runtime_error(SDL_GetError()); */
	/* } */

	image.reset(SDL_ConvertSurfaceFormat(image.get(), SDL_PIXELFORMAT_ABGR8888, 0)); 

	if (image == nullptr)
	{
		std::string msg{ "Failed to load image from file: " };
		msg.append(filename);
		msg.append("\n\tDue to: ");
		msg.append(IMG_GetError());

		throw std::runtime_error(msg);
	}

	return image;
}

auto run() -> void
{
	std::basic_string<GLchar> vertexShaderSource = LoadShaderSource("assets/vertex.glsl");
	std::basic_string<GLchar> fragmentShaderSource = LoadShaderSource("assets/fragment.glsl");

	WavefrontObject cubeObj;
	{
		std::ifstream cubeFile{ "assets/cube.obj" };
		cubeObj = WavefrontParser<>::ParseObject(cubeFile);
		cubeFile.close();
	}

	// Initialize SDL2_image
	IMG_Init(IMG_INIT_PNG);
	ScopeGuard sdlImageGuard{ IMG_Quit };

	auto texture05Purple = LoadImage("assets/kenney_prototypetextures/PNG/Purple/texture_05.png");

	// Initialize glfw
	if (glfwInit() == GLFW_FALSE)
	{
		throw std::runtime_error("Failed to init glfw!");
	}
	// Terminate glfw when glfwGuard goes out of scope.
	ScopeGuard glfwGuard{ glfwTerminate };

	// Set a hint for the next glfwCreateWindowCall.
	// Makes the window invisible by default.
	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	// Make window float on i3
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	std::unique_ptr<GLFWwindow, void (*)(GLFWwindow*)> window{
		glfwCreateWindow(WinWidth, WinHeight, WinTitle, nullptr, nullptr),
		// Custom deleter, so we don't leak memory, even if an exception is thrown.
		glfwDestroyWindow
	};

	if (!window)
	{
		throw std::runtime_error("Failed to create window!");
	}

	// Make OpenGL context of window the current in this thread.
	glfwMakeContextCurrent(window.get());

	// Init GLEW: Loads addresses of OpenGL functions.
	if (GLenum status = glewInit(); status != GLEW_OK)
	{
		throw std::runtime_error("Failed to init GLEW, with status: " + std::to_string(status));
	}

	if (!GLEW_ARB_explicit_uniform_location)
	{
		throw std::runtime_error("GL_ARB_explicit_uniform_location is required");
	}
	// Load and build shaders.
	ShaderProgram program{ std::array<ShaderObject, 2>{
		{ { ShaderObject::Type::Vertex, vertexShaderSource },
		  { ShaderObject::Type::Fragment, fragmentShaderSource } } } };

	program.Bind();

	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	std::cout << "Pixel format: " << SDL_GetPixelFormatName(texture05Purple->format->format) << '\n';

	SDL_LockSurface(texture05Purple.get());
	glTexImage2D(GL_TEXTURE_2D,
			0, //level of detail
			GL_RGBA, // GPU format 
			texture05Purple->w, //width
			texture05Purple->h, //height
			0, // must be 0. Legacy
			GL_RGBA, // Source format
			GL_UNSIGNED_BYTE, //pixel data type
			texture05Purple->pixels);
	SDL_UnlockSurface(texture05Purple.get());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	VertexArray vao{};
	vao.Bind();

	MeshRef sky{};
	std::vector<MeshRef> meshRefs;
	VertexBuffer<VertexInput> vbo{};
	{
		GLint start{ 0 };
		std::vector<VertexInput> buffer{};

		for (const WavefrontObject::Element<WavefrontObject::Face>& face : cubeObj.faces)
		{

			for (std::size_t i = 0; i < face.data.vertexIndexes.size(); i++) 
			{
				VertexInput vertexInput{};
				
				vertexInput.position = Vector3
				{ 
					cubeObj.vertices.at(face.data.vertexIndexes[i] - 1).data
				};

				vertexInput.texturePosition = Vector2
				{
					cubeObj.textureVertices.at(face.data.textureVertexIndexes[i] - 1).data
				};

				buffer.push_back(vertexInput);
			}

			GLint vertexCount{ static_cast<GLint>(face.data.vertexIndexes.size()) };
			meshRefs.push_back({ GL_TRIANGLE_FAN, start, vertexCount });
			start += vertexCount; 
		}

		vbo.AllocateBuffer(buffer, GL_STATIC_DRAW);
		vao.EnableAttributeArray(0);
		vao.EnableAttributeArray(1);
		vao.SetAttributePointers(vbo, 0);
	}

	/// Function to draw an GL_ARRAY_BUFFER
	auto drawArrays = [&program](const MeshRef& meshRef, Matrix4x4 model, Color color) -> void
	{
		program.SetUniform(0, model, false);
		program.SetUniform(3, color);

		glDrawArrays(meshRef.mode, meshRef.first, meshRef.count);
	};

	glEnable(GL_DEPTH_TEST);

	// Make window visible.
	glfwShowWindow(window.get());

	std::chrono::time_point<std::chrono::system_clock> startTime
	{
		std::chrono::system_clock::now()
	};

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	float aspect = static_cast<float>(WinHeight) / static_cast<float>(WinWidth);
	program.SetUniform(2, Matrix4x4::Perspective(mathf::Pi / 2.0f, aspect, 2.0f, 100.0f));
	program.SetUniform(4, 0);

	float deltaTimeMilli{ 1.0f / 60.0f };
	float angle{ 0.0f };
	while (glfwWindowShouldClose(window.get()) == GLFW_FALSE)
	{
		// Process OS events, like keyboard/mouse input, close, resize requests etc.
		glfwPollEvents();

		// Set the color with which to clear the color buffer.
		glClearColor(
			SkyColor.RedClamped(),
			SkyColor.GreenClamped(),
			SkyColor.BlueClamped(),
			SkyColor.AlphaClamped());
		// Clear only the color buffer.
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		angle += 0.1f * mathf::Pi / deltaTimeMilli;

		Vector3 cameraPos {
			Matrix4x4::Rotate({ 0.0f, angle, 0.0f }) *
			Vector4{ 0.0f, 0.3f, -0.5f, 1.0f }
		};

		/* auto viewMatrix = Matrix4x4::LookAt(cameraPos, {}, { 0.0f, 1.0f, 0.0f }); */
		auto viewMatrix = Matrix4x4::Translate({ 0.0f, 0.0f, -5.0f });

		program.SetUniform(1, viewMatrix);

		auto cubeModel = Matrix4x4::Translate({ 0.0f, 0.0f, 0.0f })
			* Matrix4x4::Rotate({ 0.0f, mathf::Pi / 4.0f, 0.0f });

		drawArrays(sky, Matrix4x4::Translate({ -1.0f, -1.0f, 0.0f }), StarColor);
		for (const MeshRef& meshRef : meshRefs)
		{
			drawArrays(meshRef, cubeModel, PlanetColor);
		}

		// Swaps the front and back buffers, i.e. show what has just been drawn,
		// and take old buffer to draw next frame.
		glfwSwapBuffers(window.get());

		std::chrono::time_point<std::chrono::system_clock> endTime{
			std::chrono::system_clock::now()
		};

		std::chrono::duration<typename decltype(endTime)::rep, typename decltype(endTime)::period>
			deltaTime{ endTime - startTime };

		deltaTimeMilli =
			std::chrono::duration_cast<std::chrono::duration<float, std::milli>>(deltaTime).count();

		startTime = endTime;
	}
}

} // namespace amn


auto PrintNestedException(const std::exception& e, std::size_t level = 0) -> void
{
	std::cerr << level << " exception: " << e.what() << '\n';

	if (const auto* systemError = dynamic_cast<const std::system_error*>(&e);
			systemError != nullptr && systemError->code())
	{
		std::cerr << level << " was caused by system error(Code "
			<< systemError->code().value();

		std::array<char, 1024> errorMsg{};
		int result = strerror_r(systemError->code().value(), errorMsg.data(), errorMsg.size());

		if (result == 0)
		{
				std::cerr << "): " << errorMsg.data() << '\n';
		}
		else
		{
			std::cerr << "): Failed to get error message for error code.\n";
		}
	}

	try
	{
		std::rethrow_if_nested(e);
	}
	catch(const std::exception& e)
	{
		PrintNestedException(e, level + 1);
	}
	catch(...)
	{
		std::cerr << level << " caught nested exception not derived from std::exception! \n";
	}
}

/// Techinically throwing exceptions from main is undefined behaviour (why???)
/// So catch them here and run the app code in another function.
auto main() -> int
{
	try
	{
		amn::run();

		return 0;
	}
	catch(std::exception& e)
	{
		std::cerr << "main(): Crashed with exception:\n";
		PrintNestedException(e);

		return 1;
	}
	catch(...)
	{
		std::cerr << "main(): Crashed with exception not derived from std::exception! \n";
	}
}
