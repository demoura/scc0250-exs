#include "Vector2.hpp"

#include <cmath>

#include "Vector3.hpp"
#include "Vector4.hpp"

namespace amn
{

Vector2::Vector2(float x, float y) : x(x), y(y) {}
Vector2::Vector2(const Vector3& vec) : x(vec.x), y(vec.y) {}
Vector2::Vector2(const Vector4& vec) : x(vec.x), y(vec.y) {}

auto Vector2::operator+=(Vector2 a) -> Vector2&
{
    x += a.x;
    y += a.y;
    return *this;
}

auto Vector2::operator-=(Vector2 a) -> Vector2&
{
    x -= a.x;
    y -= a.y;
    return *this;
}

auto Vector2::operator+=(float a) -> Vector2&
{
    x += a;
    y += a;
    return *this;
}

auto Vector2::operator-=(float a) -> Vector2&
{
    x -= a;
    y -= a;
    return *this;
}

auto Vector2::operator*=(float a) -> Vector2&
{
    x *= a;
    y *= a;
    return *this;
}

auto Vector2::operator/=(float a) -> Vector2&
{
    x /= a;
    y /= a;
    return *this;
}

auto Vector2::operator-() const -> Vector2
{
    return {-x, -y};
}

auto Vector2::SqrMagnitude() const -> float
{
    return x * x + y * y; 
}

auto Vector2::Magnitude() const -> float
{
    return std::sqrt(x * x + y * y); 
}

auto Vector2::Normalize() -> Vector2&
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        *this /= mag;        
    }

    return *this;
}

auto Vector2::Normalized() const -> Vector2
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        return *this / mag;        
    }

    return {};
}

auto operator+(Vector2 a, Vector2 b) -> Vector2
{
    return {a.x + b.x, a.y + b.y};
}

auto operator-(Vector2 a, Vector2 b) -> Vector2
{
    return {a.x - b.x, a.y - b.y};
}

auto operator*(Vector2 a, float b) -> Vector2
{
    return {a.x * b, a.y * b};
}

auto operator*(float a, Vector2 b) -> Vector2
{
    return b * a;
}

auto operator/(Vector2 a, float b) -> Vector2
{
    return {a.x / b, a.y / b};
}

auto operator==(Vector2 a, Vector2 b) -> bool
{
    return a.x == b.x && a.y == b.y;
}

auto operator!=(Vector2 a, Vector2 b) -> bool
{
    return a.x != b.x || a.y != b.y;
}

} // namespace amn
