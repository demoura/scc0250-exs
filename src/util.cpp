#include "util.hpp"

#include <string>
#include <cmath>
#include <fstream>
#include <iostream>

namespace amn
{

/// Loads a shader source from a file.
auto LoadShaderSource(const char* filename) -> std::basic_string<GLchar>
{
	std::cout << "Loading shader source from: " << filename << "\n";

	std::basic_ifstream<GLchar> file{ filename, std::ios::binary };

	if (!file.is_open())
	{
		throw std::runtime_error{ std::string{ "Failed to open file: " } + filename };
	}

	std::basic_string<GLchar> shaderSource{ std::istreambuf_iterator<GLchar>{ file },
									  std::istreambuf_iterator<GLchar>{} };

	std::cout << "Loaded " << shaderSource.size() * sizeof(GLchar) << " bytes of shader source.\n";

	return shaderSource;
}

/// Creates and compiles a shader.
auto BuildShader(GLenum shaderType, std::basic_string<GLchar> shaderSource) -> GLuint
{
	std::cout << "Compiling shader of type: " << shaderType << "\n";

	GLuint shader = glCreateShader(shaderType);
	GLchar* source = shaderSource.data();
	glShaderSource(shader, 1, &source, nullptr);
	glCompileShader(shader);

	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint logLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength < 0)
		{
			logLength = 0;
		}

		std::basic_string<GLchar> log{};
		log.resize(static_cast<size_t>(logLength));

		glGetShaderInfoLog(shader, logLength, nullptr, log.data());

		glDeleteShader(shader);

		throw std::runtime_error(
			"Failed to compile shader, of type: " + std::to_string(shaderType)
			+ ", due to: " + log);
	}

	std::cout << "Successfully compiled shader.\n";

	return shader;
}

/// Creates and links a program.
auto BuildProgram(const std::vector<GLuint>& shaders) -> GLuint
{
	GLuint program = glCreateProgram();

	for (GLuint shader : shaders)
	{
		glAttachShader(program, shader);
	}

	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint logLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength < 0)
		{
			logLength = 0;
		}

		std::basic_string<GLchar> log{};
		log.resize(static_cast<size_t>(logLength));

		glGetProgramInfoLog(program, logLength, nullptr, log.data());

		glDeleteProgram(program);

		throw std::runtime_error("Failed to link shaders, due to: " + log);
	}

	return program;
}

auto GenerateCircle(Vector2 origin, float radius) -> std::array<Vector2, 32>
{
	// Generate points for a planet.
	std::array<Vector2, 32> circle{};

	float angle = 0.0f;
	for (std::size_t i = 0; i < circle.size(); i++)
	{
		angle += (2.0f * static_cast<float>(mathf::Pi)) / static_cast<float>(circle.size());
		float x = std::cos(angle) * radius;
		float y = std::sin(angle) * radius;
		circle[i].x = x;
		circle[i].y = y;
		circle[i] += origin;
	}

	return circle;
}

} // namespace amn
