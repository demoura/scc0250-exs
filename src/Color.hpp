#ifndef AMN_COLOR_HPP
#define AMN_COLOR_HPP

#include <array>
#include <cstdint>
#include <GL/glew.h>

namespace amn
{

union Color
{
	constexpr explicit Color(std::uint32_t rgbaColor) : rgbaInt(rgbaColor) {}

	[[nodiscard]] auto RedClamped() const -> GLclampf;
	[[nodiscard]] auto GreenClamped() const -> GLclampf;
	[[nodiscard]] auto BlueClamped() const -> GLclampf;
	[[nodiscard]] auto AlphaClamped() const -> GLclampf;

private:
	std::uint32_t rgbaInt;
	std::array<std::uint8_t, 4> rgbaBytes;
};

} // namespace amn
#endif /* end of include guard: AMN_COLOR_HPP */
