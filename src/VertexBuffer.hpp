#ifndef AMN_VERTEX_BUFFER_HPP
#define AMN_VERTEX_BUFFER_HPP

#include "GLObject.hpp"

#include <GL/glew.h>
#include <type_traits>

namespace amn
{

template<typename T>
class VertexBuffer : public GLObject
{
public:
	static_assert(std::is_standard_layout_v<T>, "T must be standard layout");
	static auto Unbind() -> void;

	VertexBuffer();
	VertexBuffer(const VertexBuffer& other) = delete;
	VertexBuffer(VertexBuffer&& other) noexcept;

	auto operator=(const VertexBuffer& other) = delete;
	auto operator=(VertexBuffer&& other) noexcept -> VertexBuffer&;

	~VertexBuffer();

	auto AllocateBuffer(std::size_t size, GLenum usage) -> void;

	template<typename Container>
	auto AllocateBuffer(Container data, GLenum usage) -> void;

	auto Bind() -> void;
};

template<typename T>
auto VertexBuffer<T>::Unbind() -> void
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template<typename T>
VertexBuffer<T>::VertexBuffer()
{
	glGenBuffers(1, &self);
}

template<typename T>
VertexBuffer<T>::~VertexBuffer()
{
	glDeleteBuffers(1, &self);
}

template<typename T>
auto VertexBuffer<T>::AllocateBuffer(std::size_t size, GLenum usage) -> void
{
	Bind();
	glBufferData(GL_ARRAY_BUFFER, size, nullptr, usage);
}

template<typename T>
auto VertexBuffer<T>::Bind() -> void
{
	glBindBuffer(GL_ARRAY_BUFFER, self);
}

template<typename T>
template<typename Container>
auto VertexBuffer<T>::AllocateBuffer(Container data, GLenum usage) -> void
{
	static_assert(
		std::is_same_v<typename Container::value_type, T>, "Container::value_type must be T");
	Bind();
	glBufferData(
		GL_ARRAY_BUFFER,
		static_cast<GLsizeiptr>(sizeof(typename Container::value_type) * data.size()),
		data.data(),
		usage);
}

} // namespace amn

#endif /* end of include guard: AMN_VERTEX_BUFFER_HPP */
