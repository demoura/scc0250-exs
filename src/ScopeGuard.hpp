#ifndef AMN_SCOPE_GUARD_HPP
#define AMN_SCOPE_GUARD_HPP

#include <functional>
#include "Vector2.hpp"

namespace amn
{

/// Helper class to run glfwTerminate automatically when function returns,
// or an exception is thrown.
class ScopeGuard
{
public:
	explicit ScopeGuard(std::function<void()> onDestroy);
	~ScopeGuard();

private:
	std::function<void()> onDestroy;
};

} // namespace amn

#endif //AMN_SCOPE_GUARD_HPP
