#ifndef AMN_SHADER_OBJECT_HPP
#define AMN_SHADER_OBJECT_HPP

#include <GL/glew.h>
#include <string>

#include "GLObject.hpp"

namespace amn
{
	
class ShaderObject : public GLObject
{
public:
	enum class Type
	{
		Compute = GL_COMPUTE_SHADER,
		Fragment = GL_FRAGMENT_SHADER,
		Geometry = GL_GEOMETRY_SHADER,
		Vertex = GL_VERTEX_SHADER,
		TesselationControl = GL_TESS_CONTROL_SHADER,
		TesselationEvaluation = GL_TESS_EVALUATION_SHADER,
	};

	ShaderObject(Type shaderType, const std::basic_string<GLchar>& shaderSource);

	~ShaderObject();
};

} /* amn */ 

#endif /* end of include guard: AMN_SHADER_OBJECT_HPP */
