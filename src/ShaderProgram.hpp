#ifndef AMN_SHADER_PROGRAM_HPP
#define AMN_SHADER_PROGRAM_HPP

#include "Color.hpp"
#include "Matrix4x4.hpp"
#include "ShaderObject.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "GLObject.hpp"
#include "TypeUtils.hpp"

#include <GL/glew.h>
#include <iostream>
#include <stdexcept>
#include <type_traits>

namespace amn
{

class ShaderProgram : public GLObject
{
public:
	template<typename Container>
	explicit ShaderProgram(Container shaderObjects);

	~ShaderProgram();

	auto Bind() -> void;

	auto SetUniform(GLint location, float value) -> void;
	auto SetUniform(GLint location, GLint value) -> void;
	auto SetUniform(GLint location, GLuint value) -> void;
	auto SetUniform(GLint location, Vector2 value) -> void;
	auto SetUniform(GLint location, Vector3 value) -> void;
	auto SetUniform(GLint location, Color value) -> void;
	auto SetUniform(GLint location, Matrix4x4 value, bool transpose = false) -> void;

	template<typename Container>
	auto SetUniform(GLint location, Container values) -> void;

	template<typename Container>
	auto SetUniform(GLint location, Container values, bool transpose)
		-> std::enable_if_t<std::is_same_v<typename Container::value_type, Matrix4x4>, void>;
};


template<typename Container>
ShaderProgram::ShaderProgram(Container shaderObjects)
{
	static_assert(std::is_same_v<typename Container::value_type, ShaderObject>,
			"shaderObjects must be container of ShaderObject");

	std::cout << "Creating program:" << self << '\n';
	self = glCreateProgram();

	for (ShaderObject& shader : shaderObjects)
	{
		std::cout << "Attaching shader" << shader.Name() << '\n';
		glAttachShader(self, shader.Name());
	}

	glLinkProgram(self);

	GLint isLinked = 0;
	glGetProgramiv(self, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint logLength = 0;
		glGetProgramiv(self, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength < 0)
		{
			logLength = 0;
		}

		std::basic_string<GLchar> log{};
		log.resize(static_cast<size_t>(logLength));

		glGetProgramInfoLog(self, logLength, nullptr, log.data());

		glDeleteProgram(self);

		throw std::runtime_error("Failed to link shaders, due to: " + log);
	}
	std::cout << "Successfully linked program: " << self << '\n';
}


template<typename Container>
auto ShaderProgram::SetUniform(GLint location, Container values) -> void
{
	Bind();

	if constexpr (std::is_same_v<typename Container::value_type, float>)
	{
		glUniform1fv(location, values.size(), values.data());
	}
	else if constexpr (std::is_same_v<typename Container::value_type, GLint>)
	{
		glUniform1iv(location, values.size(), values.data());
	}
	else if constexpr (std::is_same_v<typename Container::value_type, GLuint>)
	{
		glUniform1uiv(location, values.size(), values.data());
	}
	else if constexpr (std::is_same_v<typename Container::value_type, Vector2>)
	{
		glUniform2fv(location, values.size(), values.data());
	}
	else if constexpr (std::is_same_v<typename Container::value_type, Vector3>)
	{
		glUniform4fv(location, values.size(), values.data());
	}
	else if constexpr (std::is_same_v<typename Container::value_type, Matrix4x4>)
	{
		glUniformMatrix4f(location, values.size(), false, values.data());
	}
	else
	{
		static_assert(AlwaysFalse<Container>, "Unsupported type");
	}
}

template<typename Container>
auto ShaderProgram::SetUniform(GLint location, Container values, bool transpose)
	-> std::enable_if_t<std::is_same_v<typename Container::value_type, Matrix4x4>, void>
{
	Bind();

	if constexpr (std::is_same_v<typename Container::value_type, Matrix4x4>)
	{
		glUniformMatrix4f(location, values.size(), transpose, values.data());
	}
	else
	{
		static_assert(AlwaysFalse<Container>, "Unsupported type");
	}
}
} // namespace amn

#endif /* end of include guard: AMN_SHADER_PROGRAM_HPP */
