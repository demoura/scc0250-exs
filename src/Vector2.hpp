#ifndef AMN_VECTOR2_HPP
#define AMN_VECTOR2_HPP

namespace amn
{

struct Vector3;
struct Vector4;

struct Vector2
{
public:
	Vector2() = default;
    Vector2(float x, float y);
	explicit Vector2(const Vector3& vec);
	explicit Vector2(const Vector4& vec);

    Vector2(const Vector2& other) = default;
	Vector2(Vector2&& other) = default;

	auto operator=(const Vector2& other) -> Vector2& = default;
	auto operator=(Vector2&& other) -> Vector2& = default;

	auto operator+=(Vector2 a) -> Vector2&;
	auto operator-=(Vector2 a) -> Vector2&;
	auto operator+=(float a) -> Vector2&;
	auto operator-=(float a) -> Vector2&;
	auto operator*=(float a) -> Vector2&;
	auto operator/=(float a) -> Vector2&;
	[[nodiscard]] auto operator-() const -> Vector2;

	[[nodiscard]] auto SqrMagnitude() const -> float;
	[[nodiscard]] auto Magnitude() const -> float;
	[[nodiscard]] auto Normalize() -> Vector2&;
	[[nodiscard]] auto Normalized() const -> Vector2;

public:
	float x{ 0.0f };
	float y{ 0.0f };
};

[[nodiscard]] auto operator+(Vector2 a, Vector2 b) -> Vector2;
[[nodiscard]] auto operator-(Vector2 a, Vector2 b) -> Vector2;
[[nodiscard]] auto operator*(Vector2 a, float b) -> Vector2;
[[nodiscard]] auto operator*(float a, Vector2 b) -> Vector2;
[[nodiscard]] auto operator/(Vector2 a, float b) -> Vector2;
[[nodiscard]] auto operator==(Vector2 a, Vector2 b) -> bool;
[[nodiscard]] auto operator!=(Vector2 a, Vector2 b) -> bool;

} // namespace amn
#endif
