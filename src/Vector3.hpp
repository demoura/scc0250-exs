#ifndef AMN_VECTOR3_HPP
#define AMN_VECTOR3_HPP

namespace amn
{

struct Vector2;
struct Vector4;

struct Vector3
{
public:
	Vector3() = default;
    Vector3(float x, float y, float z);
	explicit Vector3(const Vector2& vec);
	explicit Vector3(const Vector4& vec);
	Vector3(const Vector2& vec, float z);

    Vector3(const Vector3& other) = default;
	Vector3(Vector3&& other) = default;

	auto operator=(const Vector3& other) -> Vector3& = default;
	auto operator=(Vector3&& other) -> Vector3& = default;

	auto operator+=(Vector3 a) -> Vector3&;
	auto operator-=(Vector3 a) -> Vector3&;
	auto operator+=(float a) -> Vector3&;
	auto operator-=(float a) -> Vector3&;
	auto operator*=(float a) -> Vector3&;
	auto operator/=(float a) -> Vector3&;
	[[nodiscard]] auto operator-() const -> Vector3;

	[[nodiscard]] auto ClampMagnitude(float max) -> Vector3;
	[[nodiscard]] auto Cross(Vector3 a) const -> Vector3;
	[[nodiscard]] auto Dot(Vector3 a) const -> float;
	[[nodiscard]] auto SqrMagnitude() const -> float;
	[[nodiscard]] auto Magnitude() const -> float;
	[[nodiscard]] auto Normalize() -> Vector3&;
	[[nodiscard]] auto Normalized() const -> Vector3;


public:
	float x{ 0.0f };
	float y{ 0.0f };
	float z{ 0.0f };
};

[[nodiscard]] auto operator+(Vector3 a, Vector3 b) -> Vector3;
[[nodiscard]] auto operator-(Vector3 a, Vector3 b) -> Vector3;
[[nodiscard]] auto operator*(Vector3 a, float b) -> Vector3;
[[nodiscard]] auto operator*(float a, Vector3 b) -> Vector3;
[[nodiscard]] auto operator/(Vector3 a, float b) -> Vector3;
[[nodiscard]] auto operator==(Vector3 a, Vector3 b) -> bool;
[[nodiscard]] auto operator!=(Vector3 a, Vector3 b) -> bool;

} // namespace amn
#endif
