#include "Vector4.hpp"

#include <cmath>

#include "Vector2.hpp"
#include "Vector3.hpp"

namespace amn
{

Vector4::Vector4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
Vector4::Vector4(Vector3 vec) : x(vec.x), y(vec.y), z(vec.z) {}
Vector4::Vector4(Vector2 vec, float z, float w) : x(vec.x), y(vec.y), z(z), w(w) {}
Vector4::Vector4(Vector3 vec, float w) : x(vec.x), y(vec.y), z(vec.z), w(w) {}

auto Vector4::operator+=(Vector4 a) -> Vector4&
{
    x += a.x;
    y += a.y;
	z += a.z;
    w += a.w;
    return *this;
}

auto Vector4::operator-=(Vector4 a) -> Vector4&
{
    x -= a.x;
    y -= a.y;
	z -= a.z;
    w -= a.w;
    return *this;
}

auto Vector4::operator+=(float a) -> Vector4&
{
    x += a;
    y += a;
    z += a;
    w += a;
    return *this;
}

auto Vector4::operator-=(float a) -> Vector4&
{
    x -= a;
    y -= a;
    z -= a;
    w -= a;
    return *this;
}

auto Vector4::operator*=(float a) -> Vector4&
{
    x *= a;
    y *= a;
	z *= a;
    w *= a;
    return *this;
}

auto Vector4::operator/=(float a) -> Vector4&
{
    x /= a;
    y /= a;
	z /= a;
    w /= a;
    return *this;
}

auto Vector4::operator-() const -> Vector4
{
    return {-x, -y, -z, -w};
}

auto Vector4::SqrMagnitude() const -> float
{
    return x * x + y * y + z * z + w * w; 
}

auto Vector4::Magnitude() const -> float
{
    return std::sqrt(x * x + y * y + z * z + w * w); 
}

auto Vector4::Normalize() -> Vector4&
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        *this /= mag;        
    }

    return *this;
}

auto Vector4::Normalized() const -> Vector4
{
    const float mag = Magnitude();

    if (mag != 0.0f)
    {
        return *this / mag;        
    }

    return {};
}

auto operator+(Vector4 a, Vector4 b) -> Vector4
{
    return {a.x + b.x, a.y + b.y, a.z + b.z, a.w - b.w };
}

auto operator-(Vector4 a, Vector4 b) -> Vector4
{
    return {a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w };
}

auto operator*(Vector4 a, float b) -> Vector4
{
    return {a.x * b, a.y * b, a.z * b, a.w * b};
}

auto operator*(float a, Vector4 b) -> Vector4
{
    return b * a;
}

auto operator/(Vector4 a, float b) -> Vector4
{
    return {a.x / b, a.y / b, a.z / b, a.w / b };
}

auto operator==(Vector4 a, Vector4 b) -> bool
{
    return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}

auto operator!=(Vector4 a, Vector4 b) -> bool
{
    return a.x != b.x || a.y != b.y || a.z != b.z || a.w != b.w;
}

}
