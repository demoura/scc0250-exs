#ifndef AMN_VECTOR4_HPP
#define AMN_VECTOR4_HPP

namespace amn
{

struct Vector2;
struct Vector3;

struct Vector4
{
public:
	Vector4() = default;
	Vector4(float x, float y, float z, float w);
	explicit Vector4(Vector3 vec);
	Vector4(Vector2 vec, float z, float w);
	Vector4(Vector3 vec, float w);

	Vector4(const Vector4& other) = default;
	Vector4(Vector4&& other) = default;

	auto operator=(const Vector4& other) -> Vector4& = default;
	auto operator=(Vector4&& other) -> Vector4& = default;

	auto operator+=(Vector4 a) -> Vector4&;
	auto operator-=(Vector4 a) -> Vector4&;
	auto operator+=(float a) -> Vector4&;
	auto operator-=(float a) -> Vector4&;
	auto operator*=(float a) -> Vector4&;
	auto operator/=(float a) -> Vector4&;
	[[nodiscard]] auto operator-() const -> Vector4;

	[[nodiscard]] auto SqrMagnitude() const -> float;
	[[nodiscard]] auto Magnitude() const -> float;
	[[nodiscard]] auto Normalize() -> Vector4&;
	[[nodiscard]] auto Normalized() const -> Vector4;

public:
	float x{ 0.0f };
	float y{ 0.0f };
	float z{ 0.0f };
	float w{ 0.0f };
};

[[nodiscard]] auto operator+(Vector4 a, Vector4 b) -> Vector4;
[[nodiscard]] auto operator-(Vector4 a, Vector4 b) -> Vector4;
[[nodiscard]] auto operator*(Vector4 a, float b) -> Vector4;
[[nodiscard]] auto operator*(float a, Vector4 b) -> Vector4;
[[nodiscard]] auto operator/(Vector4 a, float b) -> Vector4;
[[nodiscard]] auto operator==(Vector4 a, Vector4 b) -> bool;
[[nodiscard]] auto operator!=(Vector4 a, Vector4 b) -> bool;

} // namespace amn
#endif
