#include "ScopeGuard.hpp"

namespace amn
{

	ScopeGuard::ScopeGuard(std::function<void()> onDestroy) : onDestroy(std::move(onDestroy)) {}
	ScopeGuard::~ScopeGuard() { onDestroy(); }
} // namespace amn
