#ifndef AMN_POISSON_DISC_SAMPLING_HPP
#define AMN_POISSON_DISC_SAMPLING_HPP

#include <cstdint>
#include <vector>

#include "Vector2.hpp"

namespace amn::PoissonDiscSampler
{
auto GeneratePoints(float radius, Vector2 sampleRegionSize, std::uint32_t numSamplesBeforeRejection = 30)
	-> std::vector<Vector2>;
}

#endif /* end of include guard: AMN_POISSON_DISC_SAMPLING_HPP */
