#ifndef AMN_TYPEUTILS_HPP
#define AMN_TYPEUTILS_HPP

namespace amn
{

template<typename T>
inline constexpr bool AlwaysFalse = false;
	
} /* amn */ 


#endif /* end of include guard: AMN_TYPEUTILS_HPP */
