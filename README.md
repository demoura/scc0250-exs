# Exercício Prático #04 - SCC0250 - Computação Gráfica.

Amanda de Moura Peres (10734522)

Para compilar:
```sh
cmake -B build -S . && cmake --build build
```

Para executar:
```sh
./build/main
```

Foi testado num sistema linux, compilado com GCC. Teoricamente, deve funcionar em outros sistemas.

Não há controles o cubo rotaciona automaticamente.
