#version 420 core
// This is only available in version 4.30, 
// but my 10 year old gpu only supports up to 4.20
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) uniform mat4 model;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 projection;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texturePosition;

layout(location = 0) out vec2 outTexturePosition;

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	outTexturePosition = texturePosition;
}
